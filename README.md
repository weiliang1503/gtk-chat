# Stand With Ukraine

The creater of this project stand with Ukraine and strongly hopes Russia to stop its bebavior against human right.

# Build

[Japanese Version Readme](https://gitlab.com/weiliang1503/gtk-chat/-/blob/master/README_jp.md)

Dependencies: gtk4, libsoup3

Build with: meson, ninja, vala, gcc
Run-time dependency: go-cqhttp

How to build:
``` shell
git clone https://gitlab.com/weiliang1503/gtk-chat.git
cd Gtk-Chat/
meson build && cd build && ninja
```

# Usage

Executable: ```build/gchat```

This program listening ```localhost:5701```, config your bot to send http-post to this port.

This program sends http requests to ```localhost:5700```, config your bot to listen on this port.

You need to make sure both the program and your bot runs okay so that you can use this program properly.
