# 建築

依存する：gtk4, libsoup3

建築必要：meson, valac, gcc, ninja

建築方：
```
git clone https://gitlab.com/weiliang1503/gtk-chat.git
cd Gtk-Chat/
meson build && cd build && ninja
```

# 使いについて

実行可能ファイル：```build/gchat```

このプログラムは ```http://localhost:5701``` で HTTP POST を聞くようにしています。貴方のロボットをこのポートに送信するように設置してください。

このプログラムは ```http://localhost:5700``` を届け先にしています。貴方のロボットが聞いているか確認してください。

機能を正常的に利用できるには，このプログラムとロボット二つ全部無事運行が必要です。
