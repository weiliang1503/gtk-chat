public class QServer : Soup.Server {

	public unowned Gtk.Stack stack;//传入的要修改的stack
	public GLib.HashTable<string, int64?> chat_list;//存取id的哈希表
	public QSender sender;
	public unowned Gtk.ListBox chat_list_box;
	public unowned Gtk.Label title_label;
	public unowned Gtk.Builder builder;

	public QServer(Gtk.Builder in_builder) {

		builder = in_builder;
		chat_list = new GLib.HashTable<string, int64?>(str_hash, str_equal);
		sender = new QSender();
		
		stack = builder.get_object("stack") as Gtk.Stack;
		chat_list_box = builder.get_object("chat_list") as Gtk.ListBox;
		title_label = builder.get_object("title_label") as Gtk.Label;

		this.add_handler("/", default_handler);

	}

	//响应HTTP POST请求的主函数
	private static void default_handler(Soup.Server server, Soup.ServerMessage msg, string path, GLib.HashTable? query) {

		//读取JSON
		var parser = new Json.Parser();

		try {
			parser.load_from_data((string)msg.get_request_body().data);
		} catch (Error e) {
			print(e.message);
		}
		
		var msg_reader = new Json.Reader(parser.get_root());

		msg_reader.read_member("post_type");
		//判断事件类型，是消息则继续处理
		unowned var post_type = msg_reader.get_string_value();
		msg_reader.end_member();

		switch (post_type) {
		
		case "message":
			//消息处理
			message_handler(server ,msg_reader);

		    break;
		default:
			break;
		}
		//HTTP返回值
		msg.set_response("text", Soup.MemoryUse.COPY, "OKAY".data);

	}

	//消息处理函数
	private static bool message_handler(Soup.Server server, Json.Reader msg_reader) {

		unowned QServer qserver = server as QServer;
		unowned var stack = qserver.stack;
		unowned var chat_list = qserver.chat_list;

		
		msg_reader.read_member("message_type");
		unowned var msg_type = msg_reader.get_string_value();
		msg_reader.end_member();
		
		int64 chat_id;

		switch (msg_type) {
		case "private":
			msg_reader.read_member("user_id");
			chat_id = msg_reader.get_int_value();
			msg_reader.end_member();
			break;
		case "group":
			msg_reader.read_member("group_id");
			chat_id = msg_reader.get_int_value();
			msg_reader.end_member();
			break;
		default:
			return false;
		}
		
		msg_reader.read_member("message");
		unowned var message = msg_reader.get_string_value();
		msg_reader.end_member();

		msg_reader.read_member("message_id");
		unowned var msg_id = msg_reader.get_int_value();
		msg_reader.end_member();

		msg_reader.read_member("sender");
		msg_reader.read_member("nickname");
		var sender_name = msg_reader.get_string_value();
		msg_reader.end_member();

		msg_reader.read_member("user_id");
		var sender_id = msg_reader.get_int_value();
		msg_reader.end_member();
		msg_reader.end_member();

		//print(message);
		
		var chat_name = msg_type + chat_id.to_string();

		if (chat_list.contains(chat_name)) {//如果已经有聊天，则添加

			//print(chat_name);
			unowned var stack_child = stack.get_child_by_name(chat_name) as Gtk.ScrolledWindow;
			append_message(stack_child, sender_name, sender_id, message, msg_id, qserver.sender);
			
		} else {//无则创建一个聊天
			
			chat_list.insert(chat_name, chat_id);
			var stack_child = new Gtk.ScrolledWindow();
			//stack_child.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC);
			var chat_box = new Gtk.Box(Gtk.Orientation.VERTICAL, 2);
			stack_child.set_child(chat_box);
			
			switch (msg_type) {
				
			case "private":
				add_chat_tape(qserver, msg_type, chat_id, sender_name, qserver.chat_list_box);
				stack.add_titled(stack_child, chat_name, sender_name);
				break;

			case "group":
				var group_name = qserver.sender.get_group_name(chat_id);
				add_chat_tape(qserver, msg_type, chat_id, group_name, qserver.chat_list_box);
				stack.add_titled(stack_child, chat_name, group_name);
				break;

			default:
				break;
			}
			//填加消息函数
			append_message(stack_child, sender_name, sender_id, message, msg_id, qserver.sender);
			
			//print(chat_name);
			
		}

		return true;
		

	}
	//向一个ScrolledWindow对象内添加消息
	private static void append_message(Gtk.ScrolledWindow stack_child, string sender_name, int64 sender_id, string message, int64 msg_id, QSender sender) {

		unowned var tmp_viewport = stack_child.get_child() as Gtk.Viewport;
		unowned Gtk.Box chat_box = tmp_viewport.get_child() as Gtk.Box;
		
		var name_label = new Gtk.Label(null);

		
		//chat_box.append(name_label);
		
		var cq_msg = new CQMessage(message);

		Idle.add(() => {
				name_label.set_markup( "<span foreground=\"#919190\"><small>" + sender_name + "</small></span>");
				name_label.set_halign(Gtk.Align.START);
				name_label.set_valign(Gtk.Align.START);
				name_label.set_selectable(true);
				
				cq_msg.handle_cq();

				sender.download_avatar("private", sender_id);

				var msg_grid = new Gtk.Grid();

				var pixbuf = new Gdk.Pixbuf.from_file(Environment.get_home_dir() + "/.cache/gchat/Avatars/" + "private" + sender_id.to_string() + ".jpg");
				var new_pixbuf = pixbuf.scale_simple(45, 45, Gdk.InterpType.BILINEAR);
				
				var sender_avatar = new Gtk.Picture.for_pixbuf(new_pixbuf);
				//sender_avatar.set_icon_size(Gtk.IconSize.LARGE);
				sender_avatar.set_can_shrink(false);

				var sender_frame = new Gtk.Frame(null);
				sender_frame.set_child(sender_avatar);
				sender_frame.set_margin_bottom(4);
				sender_frame.set_margin_top(4);
				sender_frame.set_margin_start(4);
				sender_frame.set_margin_end(4);
				
				//sender_frame.set_halign(Gtk.Align.START);
				//sender_frame.set_vexpand(false);

				var sender_box = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
				sender_box.append(sender_frame);
				//sender_box.set_halign(Gtk.Align.END);
				sender_box.set_valign(Gtk.Align.END);
				
				msg_grid.attach(sender_box, 0, 0, 1, 2);
				msg_grid.attach(name_label, 1, 0, 1, 1);
				msg_grid.attach(cq_msg.out_box, 1, 1, 1, 1);

				chat_box.append(msg_grid);

		
				//将自动滚动加入事件
				scroll_window(stack_child);
				return false;
			},Priority.HIGH_IDLE);

		scroll_window(stack_child);
	}
	//发送消息函数
	public bool send_msg(string chat_name, string msg) {

		if (msg == null) {
			return false;
		}

		
		var c = chat_name[0:1];
		//print(c);
		var id = chat_list.lookup(chat_name);
		
		string type;
		
		switch (c) {
		case "p":
			type = "private";
			break;

		default:
			type = "group";
			break;
		}


		var re = sender.send_msg(type, id, msg);

		if (re == 0) {
			print("SEND FAIL!\n");
			return false;
		}

		print(re.to_string() + "\n");
		unowned var stack_child = stack.get_child_by_name(chat_name) as Gtk.ScrolledWindow;

		
		var reader = sender.get_msg_json_reader(re);
		//while (reader == null) {
		//	reader = sender.get_msg_json_reader(re);
		//}
		
		reader.read_member("message");
		var remote_msg = reader.get_string_value();
		reader.end_member();
		
		print(remote_msg+"\n");
		
		Idle.add(() => {
				append_message(stack_child, sender.get_self_nickname(), sender.get_self_user_id(), remote_msg, re, sender);
		
				return false;
			}, Priority.HIGH_IDLE);

		scroll_window(stack_child);


		return true;
	}

	private static void scroll_window(Gtk.ScrolledWindow stack_child) {

		Idle.add(() => {
				unowned var adj = stack_child.get_vadjustment();

				if ((adj.get_upper() - (adj.get_value() + adj.get_page_size())) <= 300) {
					print("do scrolling");
					adj.set_value(adj.get_upper());
				}
				
				return false;
			},Priority.DEFAULT_IDLE);
	}

	private static void add_chat_tape(QServer server, string type, int64 id, string chat_title, Gtk.ListBox chat_list_box) {

		unowned var sender = server.sender;

		var chat_tape = new ListGrid();

		Idle.add(() => {
				sender.download_avatar(type, id);
				var pixbuf = new Gdk.Pixbuf.from_file(Environment.get_home_dir() + "/.cache/gchat/Avatars/" + type + id.to_string() + ".jpg");
				var new_pixbuf = pixbuf.scale_simple(45, 45, Gdk.InterpType.BILINEAR);
				var chat_avatar = new Gtk.Picture.for_pixbuf(new_pixbuf);
				var chat_avatar_frame = new Gtk.Frame(null);

				chat_avatar.set_halign(Gtk.Align.START);
				chat_avatar.set_valign(Gtk.Align.START);
				chat_avatar.set_can_shrink(false);
				chat_avatar_frame.set_child(chat_avatar);
				chat_avatar_frame.set_margin_bottom(4);
				chat_avatar_frame.set_margin_top(4);
				chat_avatar_frame.set_margin_start(4);
				chat_avatar_frame.set_margin_end(4);

				var chat_name_label = new Gtk.Label(chat_title);

				chat_tape.chat_name = type + id.to_string();
				chat_tape.chat_title = chat_title;
				chat_tape.type = type;
				chat_tape.id = id;
				
				chat_tape.attach(chat_avatar_frame, 0, 0, 1, 1);
				chat_tape.attach(chat_name_label, 1, 0, 1, 1);

				chat_list_box.append(chat_tape);
				return false;
			},Priority.DEFAULT_IDLE);

	}

	public void init_friend_list(Gtk.ListBox friend_list) {

		var reader = sender.get_friend_list();

		var list_length = reader.count_elements();

		for (var i = 0; i < list_length; i++) {
			
			reader.read_element(i);
			reader.read_member("user_id");
			var user_id = reader.get_int_value();
			reader.end_member();
			reader.read_member("nickname");
			var nickname = reader.get_string_value();
			reader.end_member();
			reader.end_element();

			add_chat_tape(this, "private", user_id, nickname, friend_list);
			
		}
		
	}

	public void init_group_list(Gtk.ListBox group_list) {

		var reader = sender.get_group_list();

		var list_length = reader.count_elements();

		for (var i = 0; i < list_length; i++) {
			
			reader.read_element(i);
			reader.read_member("group_id");
			var group_id = reader.get_int_value();
			reader.end_member();
			reader.read_member("group_name");
			var group_name = reader.get_string_value();
			reader.end_member();
			reader.end_element();

			add_chat_tape(this, "group", group_id, group_name, group_list);
			
		}
		
	}

	public void go_to_chat(string chat_name, string type, int64 id, string chat_title) {

		if (chat_list.contains(chat_name)) {

			stack.set_visible_child_name(chat_name);
			
		} else {

			add_chat_tape(this, type, id, chat_title, chat_list_box);
			add_chat(chat_list, chat_name, id, chat_title, stack);
			stack.set_visible_child_name(chat_name);
			title_label.set_text(chat_title);

		}
		
	}

	private void add_chat(HashTable<string ,int64?> chat_list, string chat_name, int64 id, string chat_title, Gtk.Stack stack) {

		chat_list.insert(chat_name, id);
		var stack_child = new Gtk.ScrolledWindow();
		//stack_child.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC);
		var chat_box = new Gtk.Box(Gtk.Orientation.VERTICAL, 2);
		stack_child.set_child(chat_box);
			
		stack.add_titled(stack_child, chat_name, chat_title);
		//stack.set_visible_child_name(chat_name);	
		
	}
	

}