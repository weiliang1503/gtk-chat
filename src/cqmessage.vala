public class CQMessage : Object {

	public string msg;
	//public Gtk.Frame out_frame;
	public Gtk.Box out_box;
	//public Gtk.Widget[] out_widgets;
	private int widget_now;
	private static Regex cq_regex = /\[CQ:.+?\]/;
	private static Regex which_cq_regex = /\[CQ:.+?,/;
	private QSender sender;
	
	public CQMessage(string in_msg) {

		//out_widgets = new Gtk.Widget[1];
		out_box = new Gtk.Box(Gtk.Orientation.VERTICAL, 1);
		//out_frame = new Gtk.Frame(null);
		//out_box = new Gtk.Box(Gtk.Orientation.VERTICAL, 1);
		//out_box.set_halign(Gtk.Align.START);
		//out_box.set_valign(Gtk.Align.START);
		
		//out_frame.set_child(frame_box);
		msg = in_msg;
		widget_now = 0;
		sender = new QSender();

	}

	public void handle_cq() {//处理CQ码

		string cq_string;
		MatchInfo mi;
		string cq_type;
		
		while (this.has_cq()) {

			cq_regex.match(msg, 0, out mi);
			cq_string = mi.fetch(0);
			which_cq_regex.match(cq_string, 0, out mi);
			cq_type = mi.fetch(0)[4:-1];
			print(cq_type + "\n");

			if (cq_type == "at") {
			    msg = msg.replace(cq_string, at_handler(cq_string));
				continue;
			}
			
			msg = msg.replace(cq_string, "");
			print(msg + "\n");
			//print(msg);
			switch (cq_type) {

			case "image":
				//print(cq_string);
				out_box.append(image_handler(cq_string));
				//widget_now += 1;
				break;

			case "reply":
				//print(cq_string);
				out_box.append(reply_handler(cq_string));
				//widget_now += 1;
				break;

			default:
				var unknown_cq_label = new Gtk.Label("UNKOWN CQ CODE: " + cq_string);
				unknown_cq_label.set_wrap(true);
				unknown_cq_label.set_wrap_mode(Pango.WrapMode.CHAR);
				out_box.append(unknown_cq_label);
				break;
			}
			

		}

		if(msg != "") {
			
			var main_label = new Gtk.Label(msg);
			main_label.set_halign(Gtk.Align.START);
			main_label.set_valign(Gtk.Align.START);
			main_label.set_wrap(true);
			main_label.set_selectable(true);
			main_label.set_wrap_mode(Pango.WrapMode.CHAR);

		    out_box.append(main_label);
		}
		
	}

	public void handle_cq_light() {//防止回复消息出现长串，去除部分CQ码
		string cq_string;
		MatchInfo mi;
		string cq_type;
		
		while (this.has_cq()) {

			cq_regex.match(msg, 0, out mi);
			cq_string = mi.fetch(0);
			which_cq_regex.match(cq_string, 0, out mi);
			cq_type = mi.fetch(0)[4:-1];
			
			if (cq_type == "at") {
				msg = cq_regex.replace(msg, msg.length, 0, at_handler(cq_string), 0);
				continue;
			} else if (cq_type == "image") {
				out_box.append(image_handler(cq_string));
				msg = cq_regex.replace(msg, msg.length, 0, "", 0);
			} else {
				msg = cq_regex.replace(msg, msg.length, 0, "[未知CQ]", 0);
			}

		}

		if(msg != "") {

			var main_label = new Gtk.Label(null);
			main_label.set_markup("<small>" + msg + "</small>");
			main_label.set_halign(Gtk.Align.START);
			main_label.set_valign(Gtk.Align.START);
			main_label.set_wrap(true);
			main_label.set_selectable(true);
			main_label.set_wrap_mode(Pango.WrapMode.CHAR);

			main_label.set_margin_start(2);
			main_label.set_margin_end(2);
			main_label.set_margin_bottom(2);
		
			out_box.append(main_label);
		}
			
	}

	public bool has_cq() {
		return cq_regex.match(msg);
	}

	public string at_handler(string cq_string) {

		var get_text_regex = /,qq=.*\]/;
		MatchInfo mi;
		get_text_regex.match(cq_string, 0, out mi);

		var at_id = mi.fetch(0)[4:-1].to_int64();
		print("DEBUG: " + at_id.to_string() + "\n");
		var nickname = sender.get_nickname(at_id);

		return "@" + nickname;

	}

	public Gtk.Frame reply_handler(string cq_string) {

		var get_id_regex = /,id=.*\]/;
		MatchInfo mi;
		get_id_regex.match(cq_string, 0, out mi);

		var reply_id = mi.fetch(0)[4:-1].to_int64();

		var reply_reader = sender.get_msg_json_reader(reply_id);


		reply_reader.read_member("sender");
		reply_reader.read_member("nickname");
		var reply_sender = reply_reader.get_string_value();
		reply_reader.end_member();
		reply_reader.end_member();

		reply_reader.read_member("message");
		var reply_msg = reply_reader.get_string_value();
		reply_reader.end_member();

		if (reply_msg == null) {
			reply_msg = "空消息/获取失败";
		}

		var reply_name_label = new Gtk.Label(null);
		reply_name_label.set_markup("<span foreground=\"#919190\"><small>" + reply_sender + "</small></span>");
		reply_name_label.set_selectable(true);
		reply_name_label.set_wrap_mode(Pango.WrapMode.CHAR);
		reply_name_label.set_wrap(true);
		reply_name_label.set_halign(Gtk.Align.START);
		reply_name_label.set_margin_start(2);
		reply_name_label.set_margin_end(2);
		reply_name_label.set_margin_top(2);

		var reply_cqmsg = new CQMessage(reply_msg);
		
		reply_cqmsg.handle_cq_light();
		reply_cqmsg.out_box.prepend(reply_name_label);

		var reply_frame = new Gtk.Frame(null);
		reply_frame.set_child(reply_cqmsg.out_box);
		reply_frame.set_halign(Gtk.Align.START);
		
		return reply_frame;
		
	}

	public Gtk.Widget image_handler(string cq_string) {

		var get_pic_name_regex = /,file=.+?,/;
		MatchInfo mi_name;
		get_pic_name_regex.match(cq_string, 0, out mi_name);

		var pic_name = mi_name.fetch(0)[6:-1];

		var get_pic_uri_regex = /,url=.+?\]/;
		MatchInfo mi_uri;
		get_pic_uri_regex.match(cq_string, 0, out mi_uri);

		var pic_uri = mi_uri.fetch(0)[5:-1];

		//print(pic_uri + "\n");
		
		Gtk.Picture pic_widget = new Gtk.Picture();

		//var loader = new PictureLoader(pic_widget, pic_name, pic_uri, sender);
		new Thread<int>.try("LoadPic", () => {

				
				sender.download_picture(pic_name, pic_uri);
				//Gtk.Picture pic_widget;
			    load_pic(pic_widget, Environment.get_home_dir() + "/.cache/gchat/Pictures/" + pic_name);

				return 0;
				
			});
		//var pic_widget = new Gtk.Image.from_pixbuf(pixbuf);
		//pic_widget.set_halign(Gtk.Align.START);
		//pic_widget.set_size_request(200, -1);
		pic_widget.set_can_shrink(false);
		pic_widget.set_valign(Gtk.Align.START);
		pic_widget.set_halign(Gtk.Align.START);

		var gesture = new Gtk.GestureClick();
		gesture.released.connect(() => {

				File pic = File.new_for_path(Environment.get_home_dir() + "/.cache/gchat/Pictures/" + pic_name);
				var uri = pic.get_uri();

				GLib.AppInfo.launch_default_for_uri(uri, null);

				
			});
		
		pic_widget.add_controller(gesture);

		var frame = new Gtk.Frame(null);

		frame.set_child(pic_widget);
		frame.set_halign(Gtk.Align.START);
		frame.set_valign(Gtk.Align.START);
		
		return frame;
		

	}
	
}

public void load_pic(Gtk.Picture pic_widget, string pic_path) {

	Gdk.Pixbuf pixbuf_new;
	var pixbuf = new Gdk.Pixbuf.from_file(pic_path);
		
	if(pixbuf.height > 200 || pixbuf.width > 200) {
		if(pixbuf.width > pixbuf.height) {
			pixbuf_new = pixbuf.scale_simple(200, pixbuf.height * 200 / pixbuf.width, Gdk.InterpType.BILINEAR);
			pic_widget.set_pixbuf(pixbuf_new);
			//pic_widget = new Gtk.Picture.for_filename(Environment.get_home_dir() + "/.cache/gchat/Pictures/" + pic_name);
			//pic_widget.set_size_request(200, pixbuf.height * 200 / pixbuf.width);
			//print("Width: %d, Height: %d \n",);
				
		} else {
			pixbuf_new = pixbuf.scale_simple(pixbuf.width * 200 / pixbuf.height, 200, Gdk.InterpType.BILINEAR);
			pic_widget.set_pixbuf(pixbuf_new);
			//pic_widget.set_size_request(pixbuf.width * 200 / pixbuf.height, 200);
		}

		//pic_widget.set_pixbuf(pixbuf_new);
	} else {
		pic_widget.set_pixbuf(pixbuf);
		//pic_widget.set_size_request(pixbuf.width, pixbuf.height);
	}
	
}