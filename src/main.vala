int main(string[] argv) {
	
    Gtk.init();
	var app = new Gtk.Application("cyou.zhaose.GtkChat", GLib.ApplicationFlags.FLAGS_NONE);

	app.activate.connect(() => {
			//自ui文件中生成窗口
			var builder = new Gtk.Builder.from_resource("/cyou/zhaose/GtkChat/ui/window.ui");
			
			var window = builder.get_object("window") as Gtk.ApplicationWindow;
			window.set_application(app);

			var avatar_frame = builder.get_object("avatar_frame") as Gtk.Frame;
			var title_label = builder.get_object("title_label") as Gtk.Label;

			var add_chat_button = builder.get_object("add_chat_button") as Gtk.Button;
			
			//检查连接
			var connection_check_sender = new QSender();
			while (!connection_check_sender.check_connection()) {
				print("无连接，请确认后端\n");
				Thread.usleep(1000000);
			}

			var user_id = connection_check_sender.get_self_user_id();
			connection_check_sender.download_avatar("private", user_id);
			
			var my_avatar = new Gtk.Picture.for_filename(Environment.get_home_dir() + "/.cache/gchat/Avatars/" + user_id.to_string() + ".jpg");
			avatar_frame.set_child(my_avatar);
			
			var stack = builder.get_object("stack") as Gtk.Stack;

			var input = builder.get_object("input") as Gtk.Entry;
			var send_button = builder.get_object("send_button") as Gtk.Button;
			var pic_button = builder.get_object("pic_button") as Gtk.Button;
			
			var chat_list_box = builder.get_object("chat_list") as Gtk.ListBox;
			
			//-----------------------
			var msg_server =  new QServer(builder);
			var input_buffer = input.get_buffer();
			//按钮发送
			send_button.clicked.connect(() => {
					var msg = input_buffer.get_text();
					input_buffer.delete_text(0, -1);
					var chat_name = stack.get_visible_child_name();
					if (msg != "") {
						msg_server.send_msg(chat_name, msg);
					}
				});
			//回车发送
			input.activate.connect(() => {
					var msg = input_buffer.get_text();
					input_buffer.delete_text(0, -1);
					var chat_name = stack.get_visible_child_name();
					if (msg != "") {
						msg_server.send_msg(chat_name, msg);
					}
				});
			//-----------------
			//当列表选中的发生变化、切换聊天
			chat_list_box.selected_rows_changed.connect(() => {

					unowned var row = chat_list_box.get_selected_row();
					unowned var chat_tape = row.get_child() as ListGrid;
					var chat_name = chat_tape.chat_name;
					
					stack.set_visible_child_name(chat_name);
					title_label.set_text(chat_tape.chat_title);

				});
			//增加未收到过消息的聊天
		    add_chat_button.clicked.connect(() => {

				    var add_chat_builder = new Gtk.Builder.from_resource("/cyou/zhaose/GtkChat/ui/list.ui");
					var add_chat_dialog = add_chat_builder.get_object("add_chat_dialog") as Gtk.Dialog;
					var friend_list = add_chat_builder.get_object("friend_list") as Gtk.ListBox;
					var group_list = add_chat_builder.get_object("group_list") as Gtk.ListBox;

					new Thread<int>.try("load_list", () => {
							msg_server.init_friend_list(friend_list);
							msg_server.init_group_list(group_list);
							//add_chat_dialog.set_parent(window);
							return 0;
						});

					friend_list.selected_rows_changed.connect(() => {

							unowned var row = friend_list.get_selected_row();
							unowned var chat_tape = row.get_child() as ListGrid;
							var chat_name = chat_tape.chat_name;
							var chat_id = chat_tape.id;
							var chat_title = chat_tape.chat_title;

							msg_server.go_to_chat(chat_name, "private", chat_id, chat_title);
							
						});

					group_list.selected_rows_changed.connect(() => {

							unowned var row = group_list.get_selected_row();
							unowned var chat_tape = row.get_child() as ListGrid;
							var chat_name = chat_tape.chat_name;
							var chat_id = chat_tape.id;
							var chat_title = chat_tape.chat_title;

							msg_server.go_to_chat(chat_name, "group", chat_id, chat_title);
							
						});
					
					add_chat_dialog.present();
					
				});

			//发送图片
			pic_button.clicked.connect(() => {

					var dialog = new Gtk.Dialog.with_buttons("发送图片", window, Gtk.DialogFlags.USE_HEADER_BAR);
					var send_pic_button = dialog.add_button("发送", Gtk.ResponseType.OK) as Gtk.Button;
					dialog.add_button("取消", Gtk.ResponseType.CANCEL) as Gtk.Button;
					var dialog_header_bar = dialog.get_header_bar();
					dialog_header_bar.set_show_title_buttons(false);
					var dialog_box = dialog.get_content_area();
					var pic_preview = new Gtk.Picture();
					pic_preview.set_size_request(200, 200);
					var select_file_button = new Gtk.Button.with_label("选择图片");
					var comment_box = new Gtk.Entry();
					dialog_box.spacing = 3;
					dialog_box.margin_top = 5;
					dialog_box.margin_bottom =5;
					dialog_box.margin_start =5;
					dialog_box.margin_end = 5;
					dialog_box.append(pic_preview);
					dialog_box.append(select_file_button);
					dialog_box.append(comment_box);
					var clipboard = Gdk.Display.get_default().get_clipboard();
					string pic_path = null;
					
				    if (clipboard.get_formats().contain_mime_type("image/png")) {
						clipboard.read_texture_async.begin(null, (obj, res) => {
								try {
									var texture = clipboard.read_texture_async.end(res);
									texture.save_to_png("/tmp/gtk_chat_tmp.png");
									pic_path = "/tmp/gtk_chat_tmp.png";
									new Thread<int>.try("LoadPic", () => {
											load_pic(pic_preview, pic_path);
											return 0;
										});
								} catch (Error e) {
									print(e.message);
								}
							});
					}

					select_file_button.clicked.connect(() => {

							var choose_file_widget = new Gtk.FileChooserDialog("选择图片", window,
																			   Gtk.FileChooserAction.OPEN,
																			   "取消", Gtk.ResponseType.CANCEL,
																			   "确定", Gtk.ResponseType.ACCEPT);

							var filter = new Gtk.FileFilter();
							filter.add_mime_type("image/png");
							filter.add_mime_type("image/jpeg");
							filter.add_mime_type("image/jpg");
							filter.add_mime_type("image/gif");

							choose_file_widget.set_filter(filter);

							choose_file_widget.response.connect((c, r) => {
									
									if (r == Gtk.ResponseType.ACCEPT) {
										pic_path = choose_file_widget.get_file().get_path();
										new Thread<int>.try("LoadPic", () => {
												load_pic(pic_preview, pic_path);
												return 0;
											});
									}

									choose_file_widget.destroy();
									
								});

							choose_file_widget.present();
							
						});
					
					send_pic_button.clicked.connect(() => {
							if (pic_path != null) {
								//new Thread<int>.try("SendMsg", () => {
								msg_server.send_msg(stack.get_visible_child_name(), "[CQ:image,file=file://" + pic_path + "]" + comment_box.get_text());
										//return 0;
										//});
							}
						});
					dialog.response.connect(dialog.destroy);
					dialog.present();
					
				});					   

			//启动后端接收消息的服务器
			new Thread<int>.try("QServerLaunch", () => {
					var loop = new MainLoop();
					try {
						msg_server.listen_all(5701, 0);
						loop.run();
					} catch (Error e) {
						print(e.message);
					}
					return 0;
				});
			
			window.present();
		});

	app.run(argv);
	
	return 0;
}