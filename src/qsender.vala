public class QSender : Object {//用于向机器人发送请求，获取信息的类

	private static string base_uri = "http://127.0.0.1:5700";
	private Soup.Session session;
	//private string msg_id_test = "";
	
	public QSender() {

		session = new Soup.Session();
		//base_uri = in_base_uri;

	}
	//发送消息
	public int64 send_msg(string type, int64 id, string msg) {

		var send_uri = base_uri;
		switch (type) {

		case "private":
			send_uri += "/send_private_msg?user_id=";
			break;

		case "group":
			send_uri += "/send_group_msg?group_id=";
			break;
		}

		send_uri += id.to_string() + "&" + Soup.Form.encode("message", msg);

		var soup_message = new Soup.Message("GET", send_uri);
		
		var parser = new Json.Parser();
		parser.load_from_stream(session.send(soup_message));
		var reader = new Json.Reader(parser.get_root());
		// print((string)soup_message.response_body.data);

		reader.read_member("status");
		var status = reader.get_string_value();
		reader.end_member();

		if (status != "ok") {
			return 0;
		}
		
		reader.read_member("data");
		reader.read_member("message_id");
		var msg_id = reader.get_int_value();
		reader.end_member();
		reader.end_member();
		
		return msg_id;
	}
	
	public string get_group_name(int64 id) {

		var send_uri = base_uri + "/get_group_info?group_id=" + id.to_string();

		var soup_message = new Soup.Message("GET", send_uri);

		//print("Getting group id for " + id.to_string());
		
		var parser = new Json.Parser();
		parser.load_from_data(((string)session.send_and_read(soup_message).get_data()).strip());
		var reader = new Json.Reader(parser.get_root());

		reader.read_member("data");
		reader.read_member("group_name");
		var group_name = reader.get_string_value();
		reader.end_member();
		reader.end_member();

		//print((string)soup_message.response_body.data);
		
		return group_name;

	}
	//获取自身昵称
	public string get_self_nickname() {

		var send_uri = base_uri + "/get_login_info";

		var soup_message = new Soup.Message("GET", send_uri);

		//print("Getting group id for " + id.to_string());
		
		var parser = new Json.Parser();
	    parser.load_from_stream(session.send(soup_message));
		var reader = new Json.Reader(parser.get_root());

		reader.read_member("data");
		reader.read_member("nickname");
		var nickname = reader.get_string_value();
		reader.end_member();
		reader.end_member();

		return nickname;
	}

	public string get_nickname(int64 id) {

		var send_uri = base_uri + "/get_stranger_info?user_id=" + id.to_string();

		var soup_message = new Soup.Message("GET", send_uri);

		//print("Getting group id for " + id.to_string());
		
		var parser = new Json.Parser();
	    parser.load_from_stream(session.send(soup_message));
		var reader = new Json.Reader(parser.get_root());

		reader.read_member("data");
		reader.read_member("nickname");
		var nickname = reader.get_string_value();
		reader.end_member();
		reader.end_member();

		return nickname;
		
	}
	//获取自身QQ号
	public int64 get_self_user_id() {

		var send_uri = base_uri + "/get_login_info";

		var soup_message = new Soup.Message("GET", send_uri);

		//print("Getting group id for " + id.to_string());
		
		var parser = new Json.Parser();
		parser.load_from_stream(session.send(soup_message));
		var reader = new Json.Reader(parser.get_root());

		reader.read_member("data");
		reader.read_member("user_id");
		var user_id = reader.get_int_value();
		reader.end_member();
		reader.end_member();

		return user_id;
	}
	//获取消息
	// public string get_msg_raw(string msg_id) {

	// 	var send_uri =new Soup.URI(base_uri + "/get_msg?" + Soup.Form.encode("message_id", msg_id));

	// 	print(send_uri.to_string(false));
		
	// 	var soup_message = new Soup.Message.from_uri("GET", send_uri);

	// 	session.send_message(soup_message);

	// 	print((string)soup_message.response_body.data);
		
	// 	var parser = new Json.Parser();
	// 	parser.load_from_data((string)soup_message.response_body.data);
	// 	var reader = new Json.Reader(parser.get_root());

	// 	reader.read_member("data");
	// 	reader.read_member("raw_message");
	// 	var msg_raw = reader.get_string_value();
	// 	reader.end_member();
	// 	reader.end_member();

	// 	return msg_raw;
	// }
	//获取消息的json reader
	public Json.Reader get_msg_json_reader(int64 msg_id) {

		// if (msg_id_test == "") {
		// 	msg_id_test = msg_id;
		// } else {
		// 	msg_id = msg_id_test;
		// }
		
		var send_uri_string = base_uri + "/get_msg?no_cache=true&" + Soup.Form.encode("message_id", msg_id.to_string());

		print("trying to get send message...\n");
		print(send_uri_string + "\n");
		
		var soup_message = new Soup.Message("GET", send_uri_string);

		var parser = new Json.Parser();
		parser.load_from_stream(session.send(soup_message));
		//parser.load_from_data(message_data);
		var reader = new Json.Reader(parser.get_root());

		reader.read_member("retcode");
		var retcode = reader.get_int_value();
		reader.end_member();

		if (retcode != 0) {
			print("failed, try again\n");
			return null;
		}

		
		reader.read_member("data");

		return reader;
		
	}
	//下载图片
	public bool download_picture(string pic_name, string pic_uri) {

		string pic_dir_path = GLib.Environment.get_home_dir() + "/.cache/gchat/Pictures";

		if (!(FileUtils.test(pic_dir_path, GLib.FileTest.IS_DIR))) {//不存在目录则创建
			File pic_dir = File.new_for_path(pic_dir_path);
			pic_dir.make_directory_with_parents();
		}

		var pic_path = pic_dir_path + "/" + pic_name;

		if (FileUtils.test(pic_path, GLib.FileTest.EXISTS)) {
			return true;
		}

		File pic = File.new_for_path(pic_path);
		var data_stream = new DataOutputStream(pic.create(FileCreateFlags.NONE));

		var soup_message = new Soup.Message("GET", pic_uri);

		var data = session.send_and_read(soup_message).get_data();

		foreach (var pic_byte in data) {

			data_stream.put_byte(pic_byte);

		}
		
		return true;
	}
	//下载头像
	public bool download_avatar(string type, int64 id) {

		string avatar_dir_path = GLib.Environment.get_home_dir() + "/.cache/gchat/Avatars";

		if (!(FileUtils.test(avatar_dir_path, GLib.FileTest.IS_DIR))) {
			File avatar_dir = File.new_for_path(avatar_dir_path);
			avatar_dir.make_directory_with_parents();
		}

		var avatar_path = avatar_dir_path + "/" + type + id.to_string() + ".jpg";

		if (FileUtils.test(avatar_path, GLib.FileTest.EXISTS)) {
			return true;
		}

		File avatar = File.new_for_path(avatar_path);
		var data_stream = new DataOutputStream(avatar.create(FileCreateFlags.NONE));

		Soup.Message soup_message;
		if (type == "private") {
			soup_message = new Soup.Message("GET", "https://q1.qlogo.cn/g?b=qq&s=100&nk=" + id.to_string());
		} else {
			soup_message = new Soup.Message("GET", "https://p.qlogo.cn/gh/" + id.to_string() + "/" + id.to_string() + "/100");
		}
	    var data = session.send_and_read(soup_message).get_data();

		foreach (var avatar_byte in data) {
			data_stream.put_byte(avatar_byte);
		}
		
		return true;
	}

	public bool check_connection() {

		var soup_message = new Soup.Message("GET", base_uri + "/get_status");

		return (session.send_and_read(soup_message).get_data() != null);

	}

	public Json.Reader get_friend_list() {

		var soup_message =  new Soup.Message("GET", base_uri + "/get_friend_list");
		var parser = new Json.Parser();
		parser.load_from_stream(session.send(soup_message));

		var reader = new Json.Reader(parser.get_root());
		reader.read_member("data");

		return reader;
		
	}

	public Json.Reader get_group_list() {

		var soup_message =  new Soup.Message("GET", base_uri + "/get_group_list");
		var parser = new Json.Parser();
		parser.load_from_stream(session.send(soup_message));

		var reader = new Json.Reader(parser.get_root());
		reader.read_member("data");

		return reader;
		
	}
	
}